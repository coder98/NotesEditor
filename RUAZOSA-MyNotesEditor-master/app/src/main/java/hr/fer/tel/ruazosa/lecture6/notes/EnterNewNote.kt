package hr.fer.tel.ruazosa.lecture6.notes

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_list_of_notes.*
import java.text.SimpleDateFormat
import java.util.*

class EnterNewNote : AppCompatActivity() {

    private var noteTitle: EditText? = null
    private var noteDescription: EditText? = null
    private var storeButton: Button? = null
    private var cancelButton: Button? = null
    private var old: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_new_note)

        noteTitle = findViewById(R.id.note_title)
        noteDescription = findViewById(R.id.note_description)

        val extra = intent.extras
        if(extra != null) {
            noteTitle?.setText(extra.get("noteTitle").toString())
            noteDescription?.setText(extra.get("noteDescription").toString())
            old = extra.get("noteTitle").toString()
        }

        storeButton = findViewById(R.id.store_button)
        storeButton?.setOnClickListener({

            if(extra != null) {

                val resultActivity = Intent()
                val noteTitle = noteTitle?.text.toString()
                val noteDescription = noteDescription?.text.toString()

                resultActivity.putExtra("noteTitle",noteTitle)
                resultActivity.putExtra("noteDescription", noteDescription)
                setResult(Activity.RESULT_OK, resultActivity)

                val note = NotesModel.Note(noteTitle = noteTitle.toString(),
                        noteDescription = noteDescription.toString(), noteTime = Date())

                if((note.noteTitle.isNullOrEmpty()) || (note.noteDescription.isNullOrEmpty())) {
                    Toast.makeText(this, "Invalid input! Please, repeat.", Toast.LENGTH_LONG).show();
                    return@setOnClickListener
                }

                val tableDao = DatabaseHelper(this).getDao(NotesModel.Note::class.java)
                var dataDelBuilder = tableDao.deleteBuilder()
                dataDelBuilder.where().eq("noteTitle", old.toString())
                dataDelBuilder.delete();


                tableDao.create(note)
                startActivity(resultActivity)

            }

            else {
                val note = NotesModel.Note(noteTitle = noteTitle?.text.toString(),
                        noteDescription = noteDescription?.text.toString(), noteTime = Date())

                if((note.noteTitle.isNullOrEmpty()) || (note.noteDescription.isNullOrEmpty())) {
                    Toast.makeText(this, "Invalid input! Please, repeat.", Toast.LENGTH_LONG).show();
                    return@setOnClickListener
                }
                val tableDao = DatabaseHelper(this).getDao(NotesModel.Note::class.java)
                tableDao.create(note)
                NotesModel.notesList.add(note)
            }
            finish()

        })

        cancelButton = findViewById(R.id.cancel_button)
        cancelButton?.setOnClickListener({
            finish()
        })
    }
}

