package hr.fer.tel.ruazosa.lecture6.notes

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.support.design.widget.FloatingActionButton
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.activity_enter_new_note.*
import kotlinx.android.synthetic.main.note_in_list.*
import java.text.SimpleDateFormat
import java.util.*

class ListOfNotes : AppCompatActivity() {

    private var fab: FloatingActionButton? = null
    private var listView: ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_of_notes)

        fab = findViewById(R.id.fab)
        fab?.setOnClickListener({
            val startEnterNewNoteIntent = Intent(this, EnterNewNote::class.java)
            startActivity(startEnterNewNoteIntent)
        })

        listView = findViewById(R.id.list_view)
        listView?.adapter = NotesAdapter(this)
    }

    inner class NotesAdapter : BaseAdapter {
        private var notesList = NotesModel.notesList
        private var context: Context? = null

        constructor(context: Context) : super() {
            this.context = context
            val tableDao = DatabaseHelper(this@ListOfNotes).getDao(NotesModel.Note::class.java)
            notesList.clear()
            NotesModel.notesList.addAll(tableDao.queryForAll())
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
            val view: View?
            val vh: ViewHolder

            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.note_in_list, parent, false)
                vh = ViewHolder(view)
                view.tag = vh
            } else {
                view = convertView
                vh = view.tag as ViewHolder
            }
            val dateFormatter = SimpleDateFormat("dd'.'MM'.'yyyy ',' HH:mm");

            val dateAsString = dateFormatter.format(notesList[position].noteTime)
            vh.noteTitle.text = notesList[position].noteTitle
            vh.noteTime.text = dateAsString

            vh.edit.setOnClickListener({
                val editNote = Intent(this@ListOfNotes, EnterNewNote::class.java)
                editNote.putExtra("noteTitle", vh.noteTitle.text.toString())
                editNote.putExtra("noteDescription", (getItem(position) as NotesModel.Note).noteDescription).toString()
                startActivityForResult(editNote,0)
            })

            vh.deleteNote.setOnClickListener({
                val tableDao = DatabaseHelper(this@ListOfNotes).getDao(NotesModel.Note::class.java)
                var dataDelBuilder = tableDao.deleteBuilder()
                dataDelBuilder.where().eq("noteTitle", vh.noteTitle.text)
                dataDelBuilder.delete();
                this.notifyDataSetChanged()
                notesList.clear()
                NotesModel.notesList.addAll(tableDao.queryForAll())
            })

            return view
        }

        override fun getItem(position: Int): Any {
            return notesList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return notesList.size
        }
    }

   override fun onActivityResult(requestCode: Int,resultCode: Int, data: Intent?) {
       super.onActivityResult(requestCode, resultCode, data)
       val notte = NotesModel.Note()
       when(resultCode) {
           Activity.RESULT_OK -> {
               notte.noteTitle = data?.getStringExtra("noteTitle")
               notte.noteDescription = data?.getStringExtra("noteDescription")

           }

       }
   }

    private class ViewHolder(view: View?) {
        val noteTime: TextView
        val noteTitle: TextView
        val edit: ImageView
        val deleteNote: ImageView

        init {
            this.noteTime = view?.findViewById<TextView>(R.id.note_time) as TextView
            this.noteTitle = view?.findViewById<TextView>(R.id.note_title) as TextView
            this.edit = view.findViewById<ImageView>(R.id.ivEdit) as ImageView
            this.deleteNote = view.findViewById<ImageView>(R.id.ivDelete) as ImageView

        }
    }
}


